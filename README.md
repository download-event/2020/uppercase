# Download Hackathon 2020 team UpperCase

## DIGITAULA, l'applicazione che rivoluziona la didattica online per le scuole!

Il repository contiene:
	- Il progetto Android-Cordova, contenuto nella cartella hackathon2020
	- I tool di firebase per gestire le funzioni del datababse
	- Il progetto, realizzato in React, relativo a un pannello di controllo del database dedicato alle segreterie delle scuole 
	- Il mockup realizzato con Adobe XD contenente i prototipi di tutte le schermate, anche quelle che non siamo riusciti a realizzare per limiti di tempo

Per utilizzare l'app è sufficiente scaricare il progetto (hackathon2020/platforms/android ) e aprirlo su Android Studio.
Una volta avviata l'applicazione le credenziali per l'accesso sono le seguenti:

	user: bonacinav@gmail.com
	password: 12345678

 
Per utilizzare il pannello di controllo è sufficiente lanciare in "react admin/simple" il comando "yarn dev". Le credenziali sono:

	user: v.bonacina@campus.unimib.it
	password: 12345678

Nel pannello di controllo non è possibile aggiungere altri utenti, operazione lasciata solo al pannello Firebase. Tuttavia è possibile modificare le impostazioni degli utenti già esistenti.
